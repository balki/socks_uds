.ONESHELL:

.SHELLFLAGS: -e

PKG_SOURCES= $(wildcard socks/*) setup.cfg README.md LICENSE

build_env/pyvenv.cfg:
	python3 -m venv build_env
	source ./build_env/bin/activate
	python3 -m pip install --upgrade pip setuptools wheel twine

make_state/latest.whl: build_env/pyvenv.cfg $(PKG_SOURCES)
	mkdir -p dist/old_versions make_state
	mv --backup=numbered dist/*whl dist/*gz dist/old_versions || true
	source ./build_env/bin/activate
	python3 setup.py sdist bdist_wheel
	cd dist
	ln -snf ../dist/*whl ../make_state/latest.whl

make_state/uploaded: make_state/latest.whl
	source ./build_env/bin/activate
	# Register on pypi.org, generate token and create a ~/.pypirc file
	# https://packaging.python.org/guides/distributing-packages-using-setuptools/#create-an-account
	twine upload dist/*gz dist/*whl
	touch make_state/uploaded

.PHONY: genpkg
genpkg: make_state/latest.whl

.PHONY: upload
upload: make_state/uploaded

.PHONY: build
build: $(PKG_SOURCES)
	python3 setup.py build

.PHONY: clean
clean:
	rm -rf build/ dist/ __pycache__/ *.egg-info/ build_env make_state

.PHONY: tests
tests:
	python socks_test.py

# TODO: does not work
# pex_tests:
# 	pex httpx -- ./socks_test.py

.PHONY: docker_tests
docker_tests:
	docker run --rm \
		-v $(PWD):/app \
		-v /var/lib/tor-shared/public.socket:/var/lib/tor-shared/public.socket \
		--network none \
		-e NO_TCP="true" \
		-w /app \
		python:alpine python socks_test.py

.PHONY: systemd_tests
systemd_tests:
	systemd-run \
		--property=PrivateNetwork=true \
		--property=User=`whoami` \
		--property=Environment=NO_TCP=true \
		--pty --same-dir --wait --collect \
		python ./socks_test.py
