import unittest
from pathlib import Path
import socks
import os
from socks.utils import check_either, start_tls

# Tor has to be configured with below options for this test
# SOCKSPort unix:/var/lib/tor-shared/public.socket WorldWritable ExtendedErrors
# SOCKSPort 9050 # default
socks_path = Path('/var/lib/tor-shared/public.socket')
socks_host = "127.0.0.1"
socks_port = 9050
domain_name = "example.com"
test_ip = "172.217.0.238"  # google.com ip

# noinspection SpellCheckingInspection
hidden_service_v2 = "facebookcorewwwi.onion"  # Facebook

# noinspection SpellCheckingInspection
hidden_service_v3 = "p53lf57qovyuvwsc6xnrppyply3vtqm7l6pcobkmyqsiofyeznfu5uqd.onion"  # Propublica news site


def no_tcp():
    return os.environ.get("NO_TCP", "false").lower() == "true"


class SocksTestCase(unittest.TestCase):
    @unittest.skipIf(no_tcp(), "No network")
    def test_tcp(self):
        url = f"http://{domain_name}"
        self.opener = socks.build_opener(socks_host=socks_host, socks_port=socks_port)
        with self.opener.open(url) as fp:
            self.assertEqual(200, fp.code)

    def test_ip(self):
        url = f"http://{test_ip}"
        self.opener = socks.build_opener(socks_path=socks_path)
        with self.opener.open(url) as fp:
            self.assertEqual(200, fp.code)
            self.assertEqual(b"<!doctype html><html", fp.read(20))

    def test_https(self):
        url = f"https://www.{domain_name}"
        self.opener = socks.build_opener(socks_path=socks_path)
        with self.opener.open(url) as fp:
            self.assertEqual(200, fp.code)

    def test_regular(self):
        url = f"http://www.{domain_name}"
        self.opener = socks.build_opener(socks_path=socks_path)
        with self.opener.open(url) as fp:
            self.assertEqual(200, fp.code)

    def test_user_pass(self):
        url = f"http://www.{domain_name}"
        self.opener = socks.build_opener(socks_path=socks_path, socks_username="foo", socks_password="bar")
        with self.opener.open(url) as fp:
            self.assertEqual(200, fp.code)

    def test_hidden_v2(self):
        url = f"http://{hidden_service_v2}"
        self.opener = socks.build_opener(socks_path=socks_path)
        with self.opener.open(url) as fp:
            self.assertEqual(200, fp.code)

    def test_hidden_v3(self):
        url = f"http://{hidden_service_v3}"
        self.opener = socks.build_opener(socks_path=socks_path)
        with self.opener.open(url) as fp:
            self.assertEqual(200, fp.code)


class SocksAsyncTestCase(unittest.IsolatedAsyncioTestCase):
    @unittest.skipIf(no_tcp(), "No network")
    async def test_aio_tcp(self):
        reader, writer = await socks.socks_connect(domain_name, 80, socks_host=socks_host, socks_port=socks_port)
        await self.do_test(reader, writer)

    async def test_aio_user_pass(self):
        reader, writer = await socks.socks_unix_connect(domain_name, 80, socks_path=socks_path, socks_username="hello",
                                                        socks_password="world")
        await self.do_test(reader, writer)

    async def test_aio(self):
        reader, writer = await socks.socks_unix_connect(domain_name, 80, socks_path=socks_path)
        await self.do_test(reader, writer)

    async def test_aio_ssl(self):
        reader, writer = await socks.socks_unix_connect(domain_name, 443, socks_path=socks_path)
        reader, writer = await start_tls(domain_name, writer.transport)
        await self.do_test(reader, writer)

    async def do_test(self, reader, writer):
        writer.write(f"GET / HTTP/1.1\r\nhost: {domain_name}\r\n\r\n".encode())
        await writer.drain()
        expected_response = b"HTTP/1.1 200 OK"
        response = await reader.read(len(expected_response))
        self.assertEqual(expected_response, response)
        writer.close()
        await writer.wait_closed()


class UtilsTestCase(unittest.TestCase):

    def test_check_either(self):
        self.assertFalse(check_either("foo", "bar"))
        self.assertTrue(check_either(None, "bar"))
        self.assertTrue(check_either("foo", None))
        self.assertFalse(check_either(None, None))
        self.assertTrue(check_either("foo", (None, None)))
        self.assertFalse(check_either((None, None), (None, None)))
        self.assertFalse(check_either(("foo", None), ("bar", None)))
        self.assertFalse(check_either(("foo", None), (None, None)))
        self.assertTrue(check_either(("foo", "bar"), (None, None)))
        self.assertFalse(check_either(("foo", "bar"), ("foo", "bar")))


@unittest.skipIf(not socks.has_httpcore(), "need httpx library for this test")
class HTTPXTestCase(unittest.IsolatedAsyncioTestCase):
    async def test_httpx(self):
        url = f"https://www.{hidden_service_v3}"
        transport = socks.AsyncProxyTransport(socks_path=socks_path)
        import httpx
        client: httpx.AsyncClient
        async with httpx.AsyncClient(transport=transport) as client:
            res = await client.get(url)
            self.assertEqual(200, res.status_code)


if __name__ == '__main__':
    unittest.main()
